import store from 'services/redux-store';
import { fetchCars } from 'actions/CarsActions';
import { loadCarBands } from '../actions/CarBrandActions';
import { loadFilteredCarsCount } from '../actions/CarFilterActions';

export default () => {
  store.dispatch(fetchCars());
  store.dispatch(loadCarBands());
  store.dispatch(loadFilteredCarsCount());
};
