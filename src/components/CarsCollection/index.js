import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import CarItem from '../CarItem/';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

const styles = theme => {
  return {
    root: theme.mixins.gutters({
      marginTop: theme.spacing.unit * 3,
      paddingTop: theme.spacing.unit * 3
    }),
    headline: theme.mixins.gutters({
      paddingTop: 16,
      paddingBottom: 16,
      marginBottom: theme.spacing.unit * 3
    }),
    loadMoreButtonContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    loadMoreButton: {
      width: '800px',
      fontWeight: 'bold',
      fontSize: '20px'
    }
  };
};

const CarsCollection = props => {
  const { classes, cars, fetchCars } = props;
  return (
    <Paper className={classes.root}>
      <Paper className={classes.headline} elevation={4}>
        <Typography
          className={classes.headlineText}
          type="headline"
          component="h3"
        >
          {props.headlineText}
        </Typography>
      </Paper>
      <Grid container spacing={24}>
        {cars.map((carData, index) => {
          return (
            <Grid key={index} item sm={2}>
              <CarItem carData={carData} />
            </Grid>
          );
        })}
        <Grid className={classes.loadMoreButtonContainer} item sm={12}>
          <Button
            raised
            color="accent"
            onClick={fetchCars}
            className={`${classes.button} ${classes.loadMoreButton}`}
          >
            Load More
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

CarsCollection.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CarsCollection);
