import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import MenuIcon from 'material-ui-icons/Menu';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import HomeIcon from 'material-ui-icons/Home';
import { Link } from 'react-router-dom';

const styles = theme => ({
  root: {
    width: '100%',
    position: 'fixed',
    top: 0,
    zIndex: 1
  },
  flex: {
    flex: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
});

function AppHeader(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            color="contrast"
            aria-label="Menu"
          >
            <HomeIcon />
          </IconButton>
          <Typography type="button" color="inherit" className={classes.flex}>
            <Button
              component={Link}
              to="/"
              className={classes.button}
              color="contrast"
            >
              Home
            </Button>
            <Button
              component={Link}
              to="/about-us"
              className={classes.button}
              color="contrast"
            >
              About Us
            </Button>
            <Button
              component={Link}
              to="/contact-us"
              className={classes.button}
              color="contrast"
            >
              Contact Us
            </Button>
          </Typography>
          <IconButton color="contrast">
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
}

AppHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppHeader);
