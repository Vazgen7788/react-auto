import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';

import BrandFilter from 'containers/CarFilters/BrandFilter';
import ModelFilter from 'containers/CarFilters/ModelFilter';
import YearStartFilter from 'containers/CarFilters/YearStartFilter';
import YearEndFilter from 'containers/CarFilters/YearEndFilter';
import PriceStartFilter from 'containers/CarFilters/PriceStartFilter';
import PriceEndFilter from 'containers/CarFilters/PriceEndFilter';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3
  }),
  searchButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchButton: {
    width: '100%',
    height: '100%'
  }
});

class CarFilter extends Component {
  render() {
    const { classes, filteredCarsCount } = this.props;

    return (
      <Paper className={classes.root}>
        <Grid container spacing={24}>
          <Grid item sm={3}>
            <BrandFilter />
          </Grid>
          <Grid item sm={4}>
            <ModelFilter />
          </Grid>
          <Grid item sm={1}>
            <YearStartFilter />
          </Grid>
          <Grid item sm={1}>
            <YearEndFilter />
          </Grid>
          <Grid item sm={1}>
            <PriceStartFilter />
          </Grid>
          <Grid item sm={1}>
            <PriceEndFilter />
          </Grid>
          <Grid className={classes.searchButtonContainer} item sm={1}>
            <Button color="primary" className={classes.searchButton} raised>
              Search {filteredCarsCount}
            </Button>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withStyles(styles)(CarFilter);
