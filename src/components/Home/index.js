import React, { Component } from 'react';
import CarFilterContainer from '../../containers/CarFilterContainer';
import CarsCollectionContainer from 'containers/CarsCollectionContainer';

import { withStyles } from 'material-ui/styles';
import Divider from 'material-ui/Divider';

const styles = theme => ({
  divider: {
    marginTop: theme.spacing.unit * 3
  }
});

class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <CarFilterContainer />
        <Divider className={classes.divider} />
        <CarsCollectionContainer />
      </div>
    );
  }
}

export default withStyles(styles)(Home);
