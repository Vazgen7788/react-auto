import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';

const styles = {
  card: {
    maxWidth: 345
  },
  media: {
    height: 200
  }
};

const CarItem = props => {
  const { classes, carData } = props;

  return (
    <div>
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={carData.image}
          title={carData.name}
        />
        <CardContent>
          <Typography type="headline" component="h2">
            {carData.name}
          </Typography>
          <Typography component="p">{carData.phone}</Typography>
        </CardContent>
      </Card>
    </div>
  );
};

CarItem.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CarItem);
