import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
});

class PageTitle extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root} >
        <Typography type="headline" component="h3">
          {this.props.children}
        </Typography>
      </Paper>
    );
  }
}

export default withStyles(styles)(PageTitle);
