import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Paper from 'material-ui/Paper';
import Select from 'material-ui/Select';
import shortid from 'shortid';

const styles = theme => ({
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  formControl: {
    width: "100%"
  }
});

const PaperSelect = ({ classes, label, options, onSelect, value }) => {
  const optionsHtml = options.map((option, index) => {
    return (
      <option value={option.value} key={shortid.generate()}>{option.name}</option>
    );
  });

  return (
    <Paper className={classes.paper}>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="age-native-simple">{label}</InputLabel>
        <Select
          native
          value={value}
          onChange={(e) => { onSelect(e.target.value) } }
          input={<Input id="age-native-simple" />}
        >
          <option value="" />
          {optionsHtml}
        </Select>
      </FormControl>
    </Paper>
  );
};

PaperSelect.propTypes = {
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};


export default withStyles(styles)(PaperSelect);
