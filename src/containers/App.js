import React from 'react';
import { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { grey, indigo } from 'material-ui/colors';

const styles = theme => ({
  root: {
    background: grey[200],
    minHeight: '100%',
    paddingTop: 80
  }
});

const theme = createMuiTheme({
  palette: {
    primary: indigo
  }
});

class App extends Component {
  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>{this.props.children}</div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(App);
