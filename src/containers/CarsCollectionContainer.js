import React from 'react';
import CarsCollection from 'components/CarsCollection/';
import { connect } from 'react-redux';
import { fetchCars } from 'actions/CarsActions';

const CarsCollectionContainer = props => <CarsCollection {...props} />;

const mapStateToProps = state => {
  const { cars } = state;
  return {
    cars,
    headlineText: 'Top Cars'
  };
};

export default connect(mapStateToProps, {
  fetchCars
})(CarsCollectionContainer);
