import React from 'react';
import { connect } from 'react-redux';
import CarFilter from '../components/CarFilter/';

const CarFilterContainer = props => <CarFilter {...props} />;

const mapStateToProps = state => {
  const { carBrands, filteredCarsCount } = state;

  return {
    filteredCarsCount,
    carBrands
  };
};

export default connect(mapStateToProps, {})(CarFilterContainer);
