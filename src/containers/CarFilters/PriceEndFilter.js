import React from 'react';
import { connect } from 'react-redux';
import { choosePriceEnd } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';
import { generatePriceOptions } from 'helpers/options-generatos';
import { priceParams } from 'config/car-filter';

const PriceEndFilter = ({ priceEndOptions, selectedPriceEnd, choosePriceEnd }) => {
  return (
    <PaperSelect
      label="Price End"
      options={priceEndOptions}
      value={selectedPriceEnd}
      onSelect={choosePriceEnd}
    />
  );
};

const priceOptions = generatePriceOptions(priceParams);

const mapStateToProps = ({ carFilter }) => {
  const { priceEnd } = carFilter;
  return {
    priceEndOptions:  priceOptions,
    selectedPriceEnd: priceEnd
  };
};

export default connect(mapStateToProps, {
  choosePriceEnd
})(PriceEndFilter);
