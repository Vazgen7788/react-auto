import React from 'react';
import { connect } from 'react-redux';
import { chooseYearStart } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';
import { yearRange } from 'config/car-filter';
import { generateYearOptions } from 'helpers/options-generatos';

const startYearOptions = generateYearOptions(yearRange.start, yearRange.end);

const YearStartFilter = ({ yearStart, startYearOptions, chooseYearStart }) => {
  return (
    <PaperSelect
      label="Year Start"
      value={yearStart}
      options={startYearOptions}
      onSelect={chooseYearStart}
    />
  );
}

const mapStateToProps = ({ carFilter: { yearStart } }) => ({
  yearStart: yearStart,
  startYearOptions
});

export default connect(mapStateToProps, {
  chooseYearStart
})(YearStartFilter);
