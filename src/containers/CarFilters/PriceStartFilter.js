import React from 'react';
import { connect } from 'react-redux';
import { choosePriceStart } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';
import { generatePriceOptions } from 'helpers/options-generatos';
import { priceParams } from 'config/car-filter';

const PriceStartFilter = ({ priceStartOptions, selectedPriceStart, choosePriceStart }) => {
  return (
    <PaperSelect
      label="Price Start"
      options={priceStartOptions}
      value={selectedPriceStart}
      onSelect={choosePriceStart}
    />
  );
};

const priceOptions = generatePriceOptions(priceParams);

const mapStateToProps = ({ carFilter }) => {
  const { priceStart } = carFilter;
  return {
    priceStartOptions:  priceOptions,
    selectedPriceStart: priceStart
  };
};

export default connect(mapStateToProps, {
  choosePriceStart
})(PriceStartFilter);
