import React from 'react';
import { connect } from 'react-redux';
import { chooseBrand } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';

const BrandFilter = ({ brandOptions, selectedBrand, chooseBrand }) => {
  return (
    <PaperSelect
      label="Brand"
      options={brandOptions}
      value={selectedBrand}
      onSelect={chooseBrand}
    />
  );
};

const mapStateToProps = ({ carBrands, carFilter }) => {
  const { brand } = carFilter;
  return {
    brandOptions: carBrands,
    selectedBrand: brand && brand.value
  };
};

export default connect(mapStateToProps, {
  chooseBrand
})(BrandFilter);
