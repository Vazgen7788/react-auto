import React from 'react';
import { connect } from 'react-redux';
import { chooseModel } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';

const ModelFilter = ({ modelOptions, selectedModel, chooseModel }) => {
  return (
    <PaperSelect
      label="Model"
      options={modelOptions}
      value={selectedModel}
      onSelect={chooseModel}
    />
  );
};

const mapStateToProps = ({ carFilter }) => {
  const { brand, model } = carFilter;
  return {
    modelOptions:  brand && brand.models ? brand.models : [],
    selectedModel: model ? model.value : undefined
  };
};

export default connect(mapStateToProps, {
  chooseModel
})(ModelFilter);
