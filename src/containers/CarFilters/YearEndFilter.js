import React from 'react';
import { connect } from 'react-redux';
import { chooseYearEnd } from 'actions/CarFilterActions';
import PaperSelect from 'components/Generic/PaperSelect/';
import { yearRange } from 'config/car-filter';
import { generateYearOptions } from 'helpers/options-generatos';

const endYearOptions = generateYearOptions(yearRange.start, yearRange.end).reverse();

const YearEndFilter = ({ yearEnd, endYearOptions, chooseYearEnd }) => {
  return (
    <PaperSelect
      label="Year End"
      value={yearEnd}
      options={endYearOptions}
      onSelect={chooseYearEnd}
    />
  );
}

const mapStateToProps = ({ carFilter: { yearEnd } }) => {
  return {
    yearEnd,
    endYearOptions
  }
};

export default connect(mapStateToProps, {
  chooseYearEnd
})(YearEndFilter);
