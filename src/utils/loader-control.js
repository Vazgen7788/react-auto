import nprogress from 'nprogress';

nprogress.configure({
  easing: 'ease',
  speed: 500,
  trickleSpeed: 100
});

const startLoader = () => {
  nprogress.start();
};

const stopLoader = () => {
  nprogress.done();
};

export default call => {
  startLoader();
  call(() => {
    stopLoader();
  });
};
