import * as types from 'constants/ActionTypes';

const filteredCarsCount = (state = 0, action) => {
  switch (action.type) {
    case types.UPDATE_FILTERED_CARS_COUNT:
      return action.count;
    default:
      return state;
  }
};

export default filteredCarsCount;
