import * as types from 'constants/ActionTypes';

const cars = (cars = [], action) => {
  switch (action.type) {
    case types.LOAD_CARS:
      return [...cars, ...action.cars];
    default:
      return cars;
  }
};

export default cars;
