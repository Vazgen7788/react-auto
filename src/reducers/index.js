import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import carFilter from './car-filter';
import carBrands from './car-brands';
import filteredCarsCount from './filtered-cars-count';
import cars from './cars';

export default combineReducers({
  carFilter,
  carBrands,
  filteredCarsCount,
  cars,
  router: routerReducer
});
