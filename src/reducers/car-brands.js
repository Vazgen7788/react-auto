import * as types from '../constants/ActionTypes';

const initialState = [];

const carBrands = (state = initialState, action) => {
  switch (action.type) {
    case types.INIT_CAR_BRANDS:
      return action.brands;
    default:
      return state;
  }
};

export default carBrands;
