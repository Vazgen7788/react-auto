import * as types from '../constants/ActionTypes';

const initialState = {
  brand: {},
  model: {},
  yearStart: undefined,
  yearEnd: undefined,
  priceStart: undefined,
  priceEnd: undefined
};

const findBrand = (value, brands = []) => {
  return brands.find(brand => {
    return brand.value === value;
  });
};

const findModel = (brand, modelValue) => {
  return brand.models.find(model => {
    return model.value === modelValue;
  });
};

const carFilter = (state = initialState, action) => {
  switch (action.type) {
    case types.SELECT_CARS_FILTER_BRAND:
      return {
        ...state,
        brand: findBrand(action.brand, action.brands),
        model: {}
      };
    case types.SELECT_CARS_FILTER_MODEL:
      return {
        ...state,
        model: findModel(state.brand, action.model)
      };
    case types.SELECT_CARS_FILTER_PRICE_START:
      return {
        ...state,
        priceStart: action.priceStart
      };
    case types.SELECT_CARS_FILTER_PRICE_END:
      return {
        ...state,
        priceEnd: action.priceEnd
      };
    case types.SELECT_CARS_FILTER_YEAR_START:
      return {
        ...state,
        yearStart: action.yearStart
      };
    case types.SELECT_CARS_FILTER_YEAR_END:
      return {
        ...state,
        yearEnd: action.yearEnd
      };
    default:
      return state;
  }
};

export default carFilter;
