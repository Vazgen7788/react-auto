import { push } from 'react-router-redux';
import _ from 'lodash';
import queryString from 'query-string';
import store from 'services/redux-store';

export default (queryParams, reset = false) => {
  const state = store.getState();
  const currentPath = _.get(state, 'router.location.pathname', '/');
  const currentQueryString = _.get(state, 'router.location.search', '');
  const currentQueryParams = queryString.parse(currentQueryString);

  const query = reset
    ? queryParams
    : Object.assign(currentQueryParams, queryParams);

  store.dispatch(push(`${currentPath}?${queryString.stringify(query)}`));
};
