export const generateYearOptions = (start, end) => {
  const options = [];

  for(let i = start; i <= end; i++) {
    options.push({
      name: i,
      value: i
    });
  }

  return options;
};

export const generatePriceOptions = ({ start, end, currency }) => {
  const options = [];

  for (let i = start; i <= end; i += 1000) {
    options.push({
      name: `${i}${currency}`,
      value: i
    });
  }

  return options;
};
