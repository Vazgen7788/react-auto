import faker from 'faker';

const TIMEOUT = 200;

export default () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(faker.random.number());
    }, TIMEOUT);
  });
};
