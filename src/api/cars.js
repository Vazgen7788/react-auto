import faker from 'faker';

const DELAY = 200;

export const loadCars = (limit = 12) => {
  const cars = [];

  while (limit--) {
    const car = faker.helpers.createCard();

    car.image = `https://loremflickr.com/320/240/car?random=${limit}`;
    cars.push(car);
  }

  return new Promise(resolve => {
    setTimeout(() => {
      resolve(cars);
    }, DELAY);
  });
};
