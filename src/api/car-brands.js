import brands from './brands.json';
const TIMEOUT = 100;

export const getCarBrands = () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(brands), TIMEOUT);
  });
}
