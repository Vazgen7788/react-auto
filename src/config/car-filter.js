// car filter year params
const countOfYears = 50;
const currentYear = (new Date()).getFullYear();
export const yearRange = {
  yearsCount: countOfYears,
  start: currentYear - countOfYears,
  end: currentYear
};


// car filer price params
export const priceParams = {
  currency: '$',
  start: 1000,
  end: 100000
};

