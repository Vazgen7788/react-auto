import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './bootstrap/store';
import bootstrap from './bootstrap/bootstrap';
import AppHeader from './components/AppHeader/';
import Home from './components/Home/';
import AboutUs from './components/AboutUs/';
import ContactUs from './components/ContactUs/';
import history from 'bootstrap/history';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import './styles/app.css';

ReactDOM.render(
  <Provider store={store}>
    <App>
      <ConnectedRouter history={history}>
        <div>
          <AppHeader />
          <div className="app-container">
            <Route exact path="/" component={Home} />
            <Route path="/about-us" component={AboutUs} />
            <Route path="/contact-us" component={ContactUs} />
          </div>
        </div>
      </ConnectedRouter>
    </App>
  </Provider>,
  document.getElementById('root')
);

bootstrap();
registerServiceWorker();
