import * as types from 'constants/ActionTypes';
import { loadCars } from 'api/cars';
import startLoader from 'utils/loader-control';

export const fetchCars = () => dispatch => {
  startLoader(stopLoader => {
    loadCars().then(cars => {
      stopLoader();
      dispatch({
        type: types.LOAD_CARS,
        cars
      });
    });
  });
};
