import * as types from '../constants/ActionTypes';
import { getCarBrands } from '../api/car-brands';

export const loadCarBands = () => dispatch => {
  getCarBrands().then((brands) => {
    dispatch({
      type: types.INIT_CAR_BRANDS,
      brands
    });
  });
};
