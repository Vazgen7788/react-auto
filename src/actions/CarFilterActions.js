import * as types from 'constants/ActionTypes';
import getCarsCount from 'api/get-cars-count';
import startLoader from 'utils/loader-control';
import pushQuery from 'helpers/push-query';

export const updateFilteredCarsCount = count => {
  return {
    type: types.UPDATE_FILTERED_CARS_COUNT,
    count
  };
};

export const loadFilteredCarsCount = () => dispatch => {
  startLoader(stopLoader => {
    getCarsCount().then(count => {
      stopLoader();
      dispatch(updateFilteredCarsCount(count));
    });
  });
};

export const chooseBrand = brand => (dispatch, getState) => {
  pushQuery({ brand });
  dispatch({
    type: types.SELECT_CARS_FILTER_BRAND,
    brand,
    brands: getState().carBrands
  });
  dispatch(loadFilteredCarsCount());
};

export const chooseModel = model => dispatch => {
  pushQuery({ model });
  dispatch({
    type: types.SELECT_CARS_FILTER_MODEL,
    model
  });
  dispatch(loadFilteredCarsCount());
};

export const chooseYearStart = yearStart => dispatch => {
  pushQuery({ yearStart });
  dispatch({
    type: types.SELECT_CARS_FILTER_YEAR_START,
    yearStart
  });
  dispatch(loadFilteredCarsCount());
};

export const chooseYearEnd = yearEnd => dispatch => {
  pushQuery({ yearEnd });
  dispatch({
    type: types.SELECT_CARS_FILTER_YEAR_END,
    yearEnd
  });
  dispatch(loadFilteredCarsCount());
};

export const choosePriceStart = priceStart => dispatch => {
  pushQuery({ priceStart });
  dispatch({
    type: types.SELECT_CARS_FILTER_PRICE_START,
    priceStart
  });
  dispatch(loadFilteredCarsCount());
};

export const choosePriceEnd = priceEnd => dispatch => {
  pushQuery({ priceEnd });
  dispatch({
    type: types.SELECT_CARS_FILTER_PRICE_END,
    priceEnd
  });
  dispatch(loadFilteredCarsCount());
};
